import logo from './logo.svg';
import './App.css';
import Clientes from "./componentes/Cliente"
import MenuPpal from "./componentes/Menu"
import Movimiento from "./componentes/Movimiento"
import Deposito from "./componentes/Deposito"
import Retiro from "./componentes/Retiro"
import Logearse from "./componentes/Logearse"
import Logout from "./componentes/Logout"
import {BrowserRouter, Route, Routes} from "react-router-dom"
function App() {
  return (
    <div className="App">
      <header className='App-header'>
        <MenuPpal/>
      </header>
      <BrowserRouter>
      <Routes>
      <Route path="/cliente" element={<Clientes />} />
      <Route path="/login" element={<Logearse />} />
      <Route path="/logout" element={<Logout />} />
      <Route path="/deposito/:id" element={<Deposito />} />
      <Route path="/retiro/:id" element={<Retiro />} />
      <Route path="/movimiento/:id" element={<Movimiento />} />
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
