import swal from "sweetalert"
import axios from "axios";
import { useState,useEffect }  from "react";
import { Link,useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/cuenta/"

let headers = {
    "usuario" : sessionStorage.getItem("usuario"),
    "clave"   : sessionStorage.getItem("clave")
  };
const Cuenta = () => {
    const navigate = useNavigate();
    const [cuentas, setCuentas] = useState([])
    useEffect(() =>{
        getCuentas()
    })

    const getCuentas = async () =>{
        //e.preventDefault();
        try {
            
            const res = await axios({
                method : "GET",
                url : URI + "consulta_cuenta?idc="+sessionStorage.getItem("usuario"),
                headers: headers 
               
            });
            
                setCuentas(res.data)
            
            //console.log(clientes)
        }
        catch (error) {
            swal("No tiene Acceso a esta Opción!", "Presiona el butón!", "error");
            navigate("/");
        }
    }


    return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <table className='table'>
                        <thead className='table-primary'>
                            <tr>
                                <th>Cuenta</th>
                                <th>Fecha Apertura</th>
                                <th>Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            { cuentas.map ( (cuenta) => (
                               
                                <tr key={ cuenta.id_cuenta}>
                                    <td> { cuenta.id_cuenta } </td>
                                    <td> { cuenta.fecha_apertura.substring(0,10) } </td>
                                    <td> { cuenta.saldo_cuenta } </td>
                                    <td>
                                        <Link to={`/deposito/${cuenta.id_cuenta}`} className='btn btn-info'><i className="fas fa-donate"></i>Depósito</Link>
                                        <Link to={`/retiro/${cuenta.id_cuenta}`} className='btn btn-warning'><i className="fas fa-money-bill-alt"></i>Retiro</Link>
                                        <Link to={`/movimiento/${cuenta.id_cuenta}`} className='btn btn-success'><i className="fas fa-file-invoice-dollar"></i>Movimientos</Link>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    );
};

export default Cuenta;